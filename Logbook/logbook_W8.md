### **Important to list out the problems first**

What are our Problems/Findings: 

Figured out that it is complicated to see the design of HAU through virtually, instead have to look for it in real time.
The measurements of real time HAU taken are not accurate due to systematic error and random error.

| Event         | Platform      | 
| ------------- |-----------|
| 1. Agenda | - Run the simulation for 2D model for the naca 0012 <br/> - Helping member on run simulation|
| 2. Goals | - run a simulation for the 2D naca 0012 model <br/> - plotting Cl vs angle of attack graph and Cd vs angle of attack |
| 3. Decision to problems above | - Using simulator to simulate the naca 0012 <br/> - plotting Cl vs angle of attack|
| 4. Method to solve the problems | - Using ANSYS Fluent to simulate the naca 0012 <br/> - using Excel to plotting the graph|
|5. Justification when solving problem | - need to consider the skewness and orthogonal qualtiy during meshing process <br/> - A refine meshing around the naca 0012 |
| 6. Impact of/after the decision chosen | - Think of the possibilities parameters involved (Lift, Drag, Thrust, Weight) <br/> - Possible CFD outcomes <br/> - to see outcomes for the Cl vs angle of attack graph and Cd vs angle of attack  |
| 7. Next Step | - starting the report on the simulation <br/> - Finding the aerodynamic vector for the HAU |

<img src="Image/image_2021-12-17_113759.png" width="900" height="400">
<img src="Image/image_2021-12-17_113840.png" width="900" height="400">
