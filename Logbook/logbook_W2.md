### **Important to list out the problems first**

What are our Problems/Findings: 

Figured out that it is complicated to see the design of HAU through virtually, instead have to look for it in real time.
The measurements of real time HAU taken are not accurate due to systematic error and random error.

| Event         | Platform      | 
| ------------- |-----------|
| 1. Agenda | -To form out the Group Progress Gantt Chart <br/> -Discuss the timeline of the progress in simulations (Real Time Measurements observing, data intake, Catia Programed Drawing or Integrated Designing, Performance Calculations of the HAU, Computational Fluid Dynamics Analysis (CFD), Ansys|
| 2. Goals | **Goals No 1:** <br/> -Looking the design of HAU in real time <br/> -Measurement intake from the modelled exact HAU <br/> Selections of the measurements in the Top, side, bottom view <br/> -Overall complete design measurements of physical HAU|
| 3. Decision to problems above | Took a visit to Lab H2.1 to look at the HAU model and the real time design|
| 4. Method to solve the problems | -Take the real measurements of the HAU which is available <br/> -Measurements: Tolerance was set according to the measurements to avoid greater significant on the error involved|
|5. Justification when solving problem | - To perform a prototype which looks good in terms of the measurements ratio set up in the CATIA programme. <br/> - To perform the goof performance calculations or reviews on HAU model <br/> - To have a good results on the Fluid Dynamics concept. |
| 6. Impact of/after the decision chosen | - Have a good illustrations or imaginations on how to design the HAU in the CATIA (Airframe Design) <br/> - Think of the possibilities parameters involved (Lift, Drag, Thrust, Weight) <br/> -Possible CFD outcomes |
| 7. Next Step | - Designing provess will begin in CATIA V5 Programme <br/> - Have to review on the parameters involved from other subsystem group to have a better idea in performance calculations |

<img src="/Image/Screenshot_20211112-153109_Photos.jpg" width="250" height="300">
