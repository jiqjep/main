### **Important to list out the problems first**

What are our Problems/Findings: 

Figured out that it is complicated to see the design of HAU through virtually, instead have to look for it in real time.
The measurements of real time HAU taken are not accurate due to systematic error and random error.

| Event         | Platform      | 
| ------------- |-----------|
| 1. Agenda | - Run the simulation for 3D model for the airship <br/> - Helping member on run simulation|
| 2. Goals | - run a simulation for the 3D HAU model <br/> - plotting volume vs weight graph |
| 3. Decision to problems above | - Using simulator to simulate the HAU <br/> - calculate relationship between volume and weight|
| 4. Method to solve the problems | - Using ANSYS Fluent to simulate the HAU <br/> - using Excel to plotting the graph|
|5. Justification when solving problem | - need to consider the skewness and orthogonal qualtiy during meshing process <br/> - A refine meshing around the airship <br/> - needs to assume total weight of the airship including payload |
| 6. Impact of/after the decision chosen | - Think of the possibilities parameters involved (Lift, Drag, Thrust, Weight) <br/> - Possible CFD outcomes <br/> - to see outcomes for the volume vs weight graph |
| 7. Next Step | - plotting Cl vs angle of attack to validate if the initial setup for Pre-Processing is correct or not |

<img src="/Image/image_2021-12-03_170905.png" width="400" height="300">
