### **Important to list out the problems first**

What are our Problems/Findings: 

Figured out that it is complicated to see the design of HAU through virtually, instead have to look for it in real time.
The measurements of real time HAU taken are not accurate due to systematic error and random error.

| Event         | Platform      | 
| ------------- |-----------|
| 1. Agenda | - Finding Aerodynamic Vector |
| 2. Goals | - Finding Aerodynamic Vector to simulate the behavioral of the HAU |
| 3. Decision to problems above | - using the equation from Syahirah Iman's Theses|
| 4. Method to solve the problems | - Using ANSYS to find terms needed |
|5. Justification when solving problem | - need to find accurate for pitching moment center, Xm |
| 6. Impact of/after the decision chosen | - need to redo the simulation for finding location for pitching moment center |
| 7. Next Step | - starting the report on the simulation <br/> - Finding the aerodynamic vector for the HAU |
