### **Important to list out the problems first**

What are our Problems/Findings: 

Figured out that it is complicated to see the design of HAU through virtually, instead have to look for it in real time.
The measurements of real time HAU taken are not accurate due to systematic error and random error.

| Event         | Platform      | 
| ------------- |-----------|
| 1. Agenda | -Discuss with Fiqri on what parameters needed for simulation <br/> - Helping member on run simulation on the cad model|
| 2. Goals | - run a simulation for the HAU model|
| 3. Decision to problems above | - Using simulator to simulate the HAU.|
| 4. Method to solve the problems | - Using ANSYS Fluent to simulate the HAU |
|5. Justification when solving problem | - need to consider the skewness and orthogonal qualtiy during meshing process <br/> - To perform a good simulation <br/> - To have a good results on the Fluid Dynamics concept. |
| 6. Impact of/after the decision chosen | - Think of the possibilities parameters involved (Lift, Drag, Thrust, Weight) <br/> - Possible CFD outcomes |
| 7. Next Step | - run the simulation <br/> - need to find on the parameters needed (based on reference book)|
