### **Important to list out the problems first**

What are our Problems/Findings: 

Figured out that it is complicated to see the design of HAU through virtually, instead have to look for it in real time.
The measurements of real time HAU taken are not accurate due to systematic error and random error.

| Event         | Platform      | 
| ------------- |-----------|
| 1. Agenda | - Finding Aerodynamic Vector <br/> - repair the airhsip's rope holder |
| 2. Goals | - Finding Aerodynamic Vector to simulate the behavioral of the HAU <br/> - repair the airhsip's rope holder to enhance its adhesion |
| 3. Decision to problems above | - using the equation from Syahirah Iman's Theses <br/> - using special glue to hold the rope holder|
| 4. Method to solve the problems | - Using ANSYS to find terms needed <br/> - using latex glue to prevent any skin expose |
|5. Justification when solving problem | - need to find accurate for pitching moment center, Xm <br/>- to prevent any minor injuries|
| 6. Impact of/after the decision chosen | - need to redo the simulation for finding location for pitching moment center <br/> -injuries prevent|
| 7. Next Step | - starting the report on the simulation |

<img src="Image/image_2022-01-09_105422.png" width="300" height="400">
