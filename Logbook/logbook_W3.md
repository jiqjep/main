### **Important to list out the problems first**

What are our Problems/Findings: 

Figured out that it is complicated to see the design of HAU through virtually, instead have to look for it in real time.
The measurements of real time HAU taken are not accurate due to systematic error and random error.

| Event         | Platform      | 
| ------------- |-----------|
| 1. Agenda | -Designing HAU on CAD software <br/> -Discuss the timeline of the progress in simulations (Real Time Measurements observing, data intake, Catia Programed Drawing or Integrated Designing, Performance Calculations of the HAU, Computational Fluid Dynamics Analysis (CFD), Ansys)|
| 2. Goals | -Designing a complete HAU based on measurement|
| 3. Decision to problems above | Using CAD software to design the HAU.|
| 4. Method to solve the problems | - Using CATIA V5/Solidworks to design the airship <br/> - Measurements: Tolerance was set according to the measurements to avoid greater significant on the error involved|
|5. Justification when solving problem | - Designing the airship using scale 1:1 on the CAD software. <br/> - To perform the goof performance calculations or reviews on HAU model <br/> - To have a good results on the Fluid Dynamics concept. |
| 6. Impact of/after the decision chosen | - Have a good illustrations or imaginations on how to design the HAU in the CATIA (Airframe Design) <br/> - Think of the possibilities parameters involved (Lift, Drag, Thrust, Weight) <br/> - Possible CFD outcomes |
| 7. Next Step | - Performance Calculation and airflow simulation can be made <br/> - Have to review on the parameters involved from other subsystem group to have a better idea in performance calculations |
 
 <img src="/Image/WhatsApp_Image_2021-11-19_at_10.39.10.jpeg" width="300" height="250">
