# Self Introduction

<img src="/Image/dp.jpg" width="300" height="200">
<!---![Halzion](/Image/dp.jpg)--->

- **Muhammad Haziq Jeffri Bin Abdullah**
- Prefered called me as "Haziq"
- 22
- Rawang, Selangor


| Strength | Weakness |
| ------ | ------ |
| Dedicate | Disorganized |
| Precise | Insecure |

### LOGBOOK
| Week | Link |
| ------ | ------ |
| 2 | [Week 2](https://gitlab.com/jiqjep/main/-/blob/main/Logbook/logbook_W3.md) |
| 3 | [Week 3](https://gitlab.com/jiqjep/main/-/blob/main/Logbook/logbook_W3.md) |
| 4 | [Week 4](https://gitlab.com/jiqjep/main/-/blob/main/Logbook/logbook_W4.md) |
| 5 | [Week 5](https://gitlab.com/jiqjep/main/-/blob/main/Logbook/logbook_W5.md) |
| 6 | [Week 6](https://gitlab.com/jiqjep/main/-/blob/main/Logbook/logbook_W6.md) |
| 7 |[Week 7](https://gitlab.com/jiqjep/main/-/blob/main/Logbook/logbook_W7.md) |
| 8 | [Week 8](https://gitlab.com/jiqjep/main/-/blob/main/Logbook/logbook_W8.md) |
| 9 | [Week 9](https://gitlab.com/jiqjep/main/-/blob/main/Logbook/logbook_W9.md) |
| 11 | [Week 11](https://gitlab.com/jiqjep/main/-/blob/main/Logbook/logbook_W11.md) |
